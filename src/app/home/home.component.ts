import { Component, OnInit } from '@angular/core';
import {Http, Response } from '@angular/http';
import { Router, ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // create link with the API
  private apiURL = 'http://api.playit.mobi/api/v1/games/getList';
  data: any = {};
  gameList: any = {};
  keys: String[];
  constructor(private http: Http, private route: ActivatedRoute,
    private router: Router) {
    // Check the incoming data
    console.log('Data incoming: ');
    this.getGameList();
    this.getData();
  }
  // map data into json format
  getData() {
    return this.http.get(this.apiURL)
    .map((res: Response) => res.json());
  }
  // subscribe data
  getGameList() {
    this.getData().subscribe(data => {
      console.log(data);
      this.data = data;
      this.gameList = data.games_list;
      this.keys = Object.keys(this.gameList);
      console.log(this.keys);
    });
  }
  gameClick(gameID: any) {
    const url: string = '/game/' + gameID;
        this.router.navigateByUrl(url);
  }
  ngOnInit() {
  }

}
