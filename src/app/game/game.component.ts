import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {Http, Response } from '@angular/http';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  constructor(private http: Http, private route: ActivatedRoute,
              private router: Router) {
              }
// create link with the API
  private apiURL = 'http://api.playit.mobi/api/v1/games/gamedata/';
  data: any = {};
  gameList: any = {};
  id: any;
  getData() {
    this.apiURL = this.apiURL + this.id;
    return this.http.get(this.apiURL)
    .map((res: Response) => res.json());
  }
  // subscribe data
  getGameInfo() {
    this.getData().subscribe(data => {
      console.log(data);
      this.data = data;
    });
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getGameInfo();
    this.getData();
  }

}
